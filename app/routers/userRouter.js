const express = require("express");

const router = express.Router();

router.get("/users", (request, response)=> {
    response.status(200).json({
        message: "GET All users"
    })
});

router.get('/users/:userId', (request, response)=> {
    let userId = request.params.userId;
    response.status(200).json({

        message: "GET user Id = " + userId
    })
});

router.post("/users", (request, response)=> {
    response.status(200).json({
        message: "Create user"
    })
});

router.put('/users/:userId', (request, response)=> {
    let userId = request.params.usersId;
    response.status(200).json({

        message: "Update user Id = " + userId
    })
});

router.delete('/users/:userId', (request, response)=> {
    let userId = request.params.userId;
    response.status(200).json({

        message: "delete user Id = " + userId
    })
});

module.exports = router;